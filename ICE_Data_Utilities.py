import requests
import pandas as pd
from pandas.tseries.offsets import BDay, MonthBegin
from dateutil import relativedelta
from io import StringIO
from util import get_log_path, set_log_level
import logging
import datetime as dt
from dateutil import parser
from dbutil import connect_to_db, dataframe_to_db, remove_data
import References


def generate_ice_urls(date, energy=True, heat_rates=True, gas=True):
    """
    # Generate the URLs to retrieve ICE data
    :param date: Date for which to generate the URL
    :param energy: Boolean to generate energy URL
    :param heat_rates: Boolean to generate heat rates URL
    :param gas: Boolean to generate gas URL
    :return: Dictionary of URL strings
    """

    # Initialize formatted date string
    day = date.day
    if day <= 9:
        day = "0" + str(day)
    else:
        day = str(day)
    month = date.month
    if month <= 9:
        month = "0" + str(month)
    else:
        month = str(month)
    year = str(date.year)
    date_string = "{year}_{month}_{day}.dat".format(year=year, month=month, day=day)

    # Generate URLs requested in the function arguments and append to the dictionary
    ice_urls = {}
    if energy is True:
        energy_url = r"https://downloads.theice.com/Settlement_Reports_CSV/Power/icecleared_power_" + date_string
        ice_urls["energy"] = energy_url
        logging.debug("Energy URL generated.")
    if heat_rates is True:
        heat_rates_url = r"https://downloads.theice.com/Settlement_Reports_CSV/Power/ngxcleared_power_" + date_string
        ice_urls["heat_rate"] = heat_rates_url
        logging.debug("Heat rate URL generated")
    if gas is True:
        gas_url = r"https://downloads.theice.com/Settlement_Reports_CSV/Gas/icecleared_gas_" + date_string
        ice_urls["gas"] = gas_url
        logging.debug("Gas URL generated")

    return ice_urls


def get_data_with_authentication(url, username, password, delimiter=","):
    """
    Use request library to initialize connection to online data sources require user authentication
    :param url: String of URL with CSV data
    :param username: Username string for authentication
    :param password: Password string for authentication
    :param delimiter: String for delimited data
    :return: DataFrame of downloaded data
    """

    # Initialize authentication string from parameters
    authentication_string = "{username}:{password}@".format(username=username, password=password)

    # Insert authentication string into URL
    url_head = url[:url.find("//")+2]
    url_bottom = url[url.find("//")+2:]
    url = url_head + authentication_string + url_bottom

    # Send a request to the URL and parse into a DataFrame
    logging.debug("Initializing request to {url}".format(url=url))
    url_data = requests.get(url)

    # If the request returns data, parse it to a DataFrame
    if url_data.status_code == 200:
        logging.debug("Retrieval success!")
        url_data = pd.read_csv(StringIO(url_data.text), delimiter=delimiter)

    # TODO: Exception handler
    else:
        url_data = False

    return url_data


def set_logging(log_level, write_to_path=False):
    """
    Initialize log level and begin logging. If no path is passed, the logger will initialize at a debug level
    :param: log_level: Configure logging based on contents of strong passed in such as 'debug' or 'WARNING'
    """

    # Get logging path from APP_LOG_ROOT environment variable and initialize logger
    if write_to_path is True:
        log_path = get_log_path()
        set_log_level(log_level, log_path)

    # Initialize logger with no log path
    else:
        set_log_level("debug", None, None)


def generate_gas_curve_constants():
    """
    Generate dates required for analyzing energy curves
    :return: Datetime objects
    """

    # Generate date relevant to analysis
    today = pd.datetime.today()

    # Last business day
    mtm_date = today - BDay(1)

    # First day of next month
    prompt_month = mtm_date + MonthBegin(1)

    # Gas prompt month
    if mtm_date > (prompt_month - BDay(3)):
        gas_prompt = prompt_month + MonthBegin(1)
    else:
        gas_prompt = prompt_month

    # Cash months as the difference in months between the gas prompt and the beginning of the month
    csh_months = relativedelta.relativedelta((mtm_date - MonthBegin(1)), gas_prompt).months

    # Term curve length
    term_curve_length = 63

    return today, mtm_date, prompt_month, gas_prompt, csh_months, term_curve_length


def get_ice_data(report_date, get_energy=True, get_heat_rates=True, get_gas=True):
    """
    Retrieve ICE data from website and populate the selected environment's IceSettlementReport table.
    :param report_date: Datetime or string for day of data to retrieve
    :param get_energy: Boolean to retrieve energy data
    :param get_heat_rates: Boolean to retrieve heat rates data
    :param get_gas: Boolean to retrieve natural gas data
    :return Dictionary of requested commodities
    """

    if type(report_date) is str:
        report_date = parser.parse(report_date)

    # Initialize dictionary to hold data
    data_dict = {}

    logging.debug("Retrieving data for {date}.".format(date=str(report_date)))

    # If the date is an ICE working day, download and process the settlement data
    if ice_date_check(report_date) is True:

        # Generate list of URLs for the date
        data_dict = generate_ice_urls(report_date, energy=get_energy, gas=get_gas, heat_rates=get_heat_rates)

        # For each ICE URL, initialize a DataFrame and add it to the dictionary
        for commodity in data_dict:

            # Generate a dictionary of DataFrame with requested commodity data
            logging.debug("Retrieving {commodity} data from URL.".format(commodity=commodity.capitalize()))
            data_dict[commodity] = get_data_with_authentication(data_dict[commodity], username="spark_settles",
                                                                password="icedata", delimiter="|")

            # If the data retrieval function returns a false, the data is not uploaded to SQL
            if data_dict[commodity] is False:
                logging.debug("{commodity} data not retrieved.".format(commodity=commodity))
                pass
            else:
                # Rename columns
                col_name_overrides = {'TRADE DATE': 'TradeDate', 'HUB': 'Hub', 'PRODUCT': 'Product',
                                      'STRIP': 'Strip', 'CONTRACT': 'Contract', 'CONTRACT TYPE': 'ContractType',
                                      'STRIKE': 'Strike', 'SETTLEMENT PRICE': 'SettlementPrice',
                                      'NET_CHANGE': 'NetChange', 'NET CHANGE': 'NetChange',
                                      'EXPIRATION DATE': 'ExpirationDate', 'PRODUCT_ID': 'ProductId',
                                      'PRODUCT ID': 'ProductId', 'Filename': 'ReportFile'}
                data_dict[commodity] = data_dict[commodity].rename(columns=col_name_overrides)

    # If the report date is past the current working day, pass
    else:
        logging.debug("{date} is not an ICE trading day".format(date=str(report_date.date())))
        pass

    return data_dict


def ice_date_check(date):
    """
    Check the datetime object to see if it's an ICE business day
    :param date: Datetime to check
    :return: Boolean value; True if the date is an ICE business day
    """

    # Dates beyond today's date cannot be retrieved
    if date > dt.datetime.now():
        return False

    # Weekend dates are not ICE trading days
    if date.weekday() >= 5:
        return False

    # TODO: Integrate holiday check
    return True


def upload_ice_settlement_report(settlement_dict, report_date, db_env):
    """
    Upload ICE settlement data to SQL and delete existing data previously uploaded for the report day
    :param settlement_dict: Dictionary of settlement data
    :param report_date: Report date to be used to delete existing data
    :param db_env: Database environment to post SQL data
    """

    # Initialize table name string
    table_name = "supply.F_IceSettlementReport"

    # Initialize connection to database
    sql_con = connect_to_db(db_env)
    logging.debug("Connected to {db_env}.".format(db_env=db_env.upper()))

    # Delete data if it has already been posted today
    remove_data(sql_con, table_name=table_name, key_col='TradeDate', keys=[report_date])
    logging.debug("Existing data in {table} removed.".format(table=table_name))

    # Iterate through the settlement data and upload it to SQL
    for commodity in settlement_dict:
        dataframe_to_db(dbcon=sql_con, df=settlement_dict[commodity], table_name=table_name)
        logging.debug("{commodity} data posted to {db_env}.{table}".format(commodity=commodity, db_env=db_env,
                                                                           table=table_name))


def generate_gas_curves(gas_data, report_date, term_curve_length, csh_months):
    """
    # Process raw data from ICE into gas curves for Oracle

    :param gas_data: DataFrame of ICE gas settlement data
    :param report_date: Day for which data was downloaded. As datetime or string.
    :param term_curve_length: Term length
    :param csh_months: Cash trading months
    :return: DataFrame of gas curve values
    """

    # Parse report date if it's a string
    if type(report_date) is str:
        report_date = parser.parse(report_date)

    # Initialize first curve period
    first_period = report_date - MonthBegin(1)

    # Initialize last curve period from curve length variable
    last_period = report_date + MonthBegin(term_curve_length + csh_months)

    # Generate a DataFrame date range
    gas_curves = pd.DataFrame()
    gas_curves["Period"] = pd.date_range(start=first_period, end=last_period, freq="M")

    # Read gas curve contract lookup table
    contract_zones = pd.read_csv("References/GasCurveContract.csv")

    # Join the gas data with curve contract information
    gas_curves = gas_data.join(contract_zones.set_index("Contract"), on="Contract", how="left")

    # Change data to datetime
    date_columns = ["TradeDate", "Strip", "ExpirationDate"]
    for column in date_columns:
        gas_curves[column] = pd.to_datetime(gas_curves[column])

    # Limit DataFrame only to contracts monitored by Spark
    gas_curves = gas_curves.dropna(subset=["Region", "Zone", "Spark Curve ID", "Type"])

    # Generate the file name of the .dat document that is the data source
    gas_url = generate_ice_urls(date=report_date, energy=False, heat_rates=False, gas=True)
    gas_url = gas_url["gas"]
    gas_curves["ReportFile"] = gas_url[len(gas_url)-29:]

    return gas_curves
