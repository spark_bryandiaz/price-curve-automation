import logging
import ICE_Data_Utilities as ice

if __name__ == "__main__":

    # Initialize logging
    ice.set_logging("debug")
    logging.debug("Logging initialized.")

    # Initialize report variables
    report_date = "11/13/17"
    db_env = "gnosisdev"

    # Initialize constants
    today, mtm_date, prompt_month, gas_prompt, csh_months, term_curve_length = ice.generate_gas_curve_constants()

    # Retrieve ICE data for all commodities
    ice_data = ice.get_ice_data(mtm_date)

    # Generate gas curves
    gas_curves = ice.generate_gas_curves(ice_data["gas"], report_date, term_curve_length, csh_months)

    # If the ice_data dictionary is not empty, post the data to SQL
    if ice_data != {}:
        ice.upload_ice_settlement_report(ice_data, report_date, db_env)
