import pandas as pd
from util import set_log_level
from dbutil import connect_to_db, dataframe_to_db, remove_data

set_log_level('debug')
pd.set_option('display.width', 180)

dbcon = connect_to_db('gnosisdev')

date_col_names = ['TRADE DATE', 'STRIP', 'EXPIRATION DATE']
df_gas = pd.read_csv(r"C:\data\icedata\icecleared_gas_2013_09_02.dat.txt", delimiter='|', parse_dates=date_col_names)
df_power = pd.read_csv(r"C:\data\icedata\icecleared_power_2013_09_02.dat.txt", delimiter='|',
                       parse_dates=date_col_names)
df_heatrates = pd.read_csv(r"C:\data\icedata\ngxcleared_power_2013_09_02.dat.txt", delimiter='|', parse_dates=date_col_names)

df_gas['Filename'] = r'icecleared_gas_2013_09_03.dat'
df_power['Filename'] = r'icecleared_power_2013_09_03.dat'
df_heatrates['Filename'] = r'ngxcleared_power_2013_09_03.dat.txt'

col_name_overrides = {'TRADE DATE': 'TradeDate', 'HUB': 'Hub', 'PRODUCT': 'Product', 'STRIP': 'Strip',
                      'CONTRACT': 'Contract', 'CONTRACT TYPE': 'ContractType', 'STRIKE': 'Strike',
                      'SETTLEMENT PRICE': 'SettlementPrice', 'NET_CHANGE': 'NetChange', 'NET CHANGE': 'NetChange',
                      'EXPIRATION DATE': 'ExpirationDate', 'PRODUCT_ID': 'ProductId', 'PRODUCT ID': 'ProductId',
                      'Filename': 'ReportFile'}

table_name = 'supply.F_IceSettlementReport'
remove_data(dbcon, table_name=table_name, key_col='TradeDate', keys=['9/2/2013'])

dataframe_to_db(dbcon, df_gas, table_name=table_name, cols_out=list(df_gas.columns),
                col_name_overrides=col_name_overrides)
dataframe_to_db(dbcon, df_power, table_name=table_name, cols_out=list(df_power.columns),
                col_name_overrides=col_name_overrides)
dataframe_to_db(dbcon, df_heatrates, table_name=table_name, cols_out=list(df_heatrates.columns),
                col_name_overrides=col_name_overrides)
